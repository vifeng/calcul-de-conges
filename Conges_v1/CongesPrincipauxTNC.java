package Conges_v1;

import commandes.*;

public class CongesPrincipauxTNC implements CommandeI<Agent,Resultat>{
    public boolean executer(Agent a, Resultat res){
        float n = 5*(float)a.getJoursTravailles().size()*(float)a.getMoisTravailles()/12;
        res.addJoursDeConges(n);
        return true;
    }
}