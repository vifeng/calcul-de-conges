package Conges_v1;

import conditions.*;

public class ConditionAgentEtranger implements ConditionI<Agent>
{
    private static boolean T = true;
    
    public boolean estSatisfaite(Agent a){
        // if(T) System.out.println("agent estEtranger : " + !a.getPaysOrigine().equals("France"));
        return !a.getPaysOrigine().equals("France");
    }
}