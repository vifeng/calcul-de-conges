package Conges_v1;

import java.time.LocalDate;

public class PeriodeConges
{
    private LocalDate debut;
    private LocalDate fin;
    
    PeriodeConges(LocalDate debut, LocalDate fin){
        this.setDebut(debut);
        this.setFin(fin);
    }
    
    public LocalDate getDebut(){
        return this.debut;
    }
    
    public LocalDate getFin(){
        return this.fin;
    }
    
    public void setDebut(LocalDate debut){
        this.debut = debut;
    }
    
    public void setFin(LocalDate fin){
        this.fin = fin;
    }
}