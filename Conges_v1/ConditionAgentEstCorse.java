package Conges_v1;

import conditions.*;

public class ConditionAgentEstCorse implements ConditionI<Agent>
{
    private static boolean T = true;
    
    public boolean estSatisfaite(Agent a){
        // if(T) System.out.println("agent estCorse : " + a.getEstCorse());
        return a.getEstCorse();
    }
}