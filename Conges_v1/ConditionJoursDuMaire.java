package Conges_v1;

import conditions.*;

public class ConditionJoursDuMaire implements ConditionI<Agent>
{
    private static boolean T = true;
    private int    nombreAnneesAncienneteRequis;
    private String nomMairie;
    private int    nombreJoursSupplementaires;
    
    public void setNombreAnneesAncienneteRequis(int nombre){
        this.nombreAnneesAncienneteRequis = nombre;
    }
    
    public void setNomMairie(String nom){
        this.nomMairie = nom;
    }
    
    public void setNombreJoursSupplementaires(int nombre){
        this.nombreJoursSupplementaires = nombre;
    }
    
    public int getNombreJoursSupplementaires(){
        return this.nombreJoursSupplementaires;
    }
    
    public boolean estSatisfaite(Agent a){
        return this.nomMairie.equals(a.getMairie().getNom()) && a.getAnciennete()>=this.nombreAnneesAncienneteRequis;
    }
}