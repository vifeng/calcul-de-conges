package Conges_v1;

public class Resultat implements java.io.Serializable
{
    private float joursDeConges;
    private float joursDeCongesSansSolde;
    private float joursDeCongesGroupes;
    private float joursDeCongesUtilises;
    
    public float getJoursDeConges(){
        return this.joursDeConges;
    }
    
    public float getJoursDeCongesSansSolde(){
        return this.joursDeCongesSansSolde;
    }
    
    public float getJoursDeCongesGroupes(){
        return this.joursDeCongesGroupes;
    }
    
    public float getJoursDeCongesUtilises(){
        return this.joursDeCongesUtilises;
    }
    
    public void setJoursDeConges(float n){
        this.joursDeConges = n;
    }
    
    public void setJoursDeCongesSansSolde(float n){
        this.joursDeCongesSansSolde = n;
    }
    
    public void setJoursDeCongesGroupes(float n){
        this.joursDeCongesGroupes = n;
    }
    
    public void setJoursDeCongesUtilises(float n){
        this.joursDeCongesUtilises = n;
    }
    
    public void addJoursDeConges(float n){
        this.joursDeConges = this.joursDeConges + n;
    }
}