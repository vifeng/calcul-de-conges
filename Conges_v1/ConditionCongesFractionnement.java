package Conges_v1;

import conditions.*;
import java.util.List;
import java.time.LocalDate;
import java.time.ZoneId;

public class ConditionCongesFractionnement implements ConditionI<Agent>
{
    private static boolean T = true;
    private String operande;
    private int jour;
    private PeriodeConges periode;
    
    public void setOperande(String operande){
        this.operande = operande;
    }
    
    public void setJour(int jour){
        this.jour = jour;
    }
    
    public void setPeriode(String[] df) {
        int now = LocalDate.now(ZoneId.of("Europe/Paris")).getYear();
        String[] d = df[0].split("/");
        String[] f = df[1].split("/");
        this.periode = new PeriodeConges(LocalDate.of(now, Integer.parseInt(d[1]), Integer.parseInt(d[0])), LocalDate.of(now, Integer.parseInt(f[1]), Integer.parseInt(f[0])));
    }
    
    public boolean estSatisfaite(Agent a){
        int n = 0;
        int now = LocalDate.now(ZoneId.of("Europe/Paris")).getYear();
       
        n = a.getJoursCongesPrisEntre(LocalDate.of(now, 1, 1), this.periode.getDebut().minusDays(1));
        n += a.getJoursCongesPrisEntre(this.periode.getFin().plusDays(1), LocalDate.of(now, 12, 31));
        
        
        // if(T) System.out.println("Jours conges pris en dehors : " + n + " (" + this.operande + this.jour + ") ");
        
        switch(this.operande) {
            case "<": return n < this.jour;
            case ">": return n > this.jour;
            case "<=": return n <= this.jour;
            case ">=": return n >= this.jour;
            case "=": return n == this.jour;
            default: return true;
        }
    }
}