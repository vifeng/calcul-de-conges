package Conges_v1;

import conditions.*;

public class ConditionTypeContrat implements ConditionI<Agent>
{
    private static boolean T = true;
    private String contrat;
    
    public void setContrat(String c){
        this.contrat = c;
    }
     
    public boolean estSatisfaite(Agent a){
        // if(T) System.out.println("TypeContrat : " + a.getTypeContrat() + ", nombre mois travaillés :  " + a.getMoisTravailles());
        return a.getTypeContrat().equals(this.contrat);
    }
}