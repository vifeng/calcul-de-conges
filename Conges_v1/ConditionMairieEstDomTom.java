package Conges_v1;

import conditions.*;

public class ConditionMairieEstDomTom implements ConditionI<Agent>
{
    private static boolean T = true;
    
    public boolean estSatisfaite(Agent a){
        Mairie m = a.getMairie();
        // if(T) System.out.println("mairie estDomTom : " + m.getEstDomTom());
        return m.getEstDomTom();
    }
}