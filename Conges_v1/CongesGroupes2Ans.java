package Conges_v1;

import commandes.*;

public class CongesGroupes2Ans implements CommandeI<Agent,Resultat>{
    public boolean executer(Agent a, Resultat res){
        res.setJoursDeCongesGroupes(a.getJoursCongesCumules());
        return true;
    }
}