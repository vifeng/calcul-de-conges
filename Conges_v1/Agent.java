package Conges_v1;

import java.util.*;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;

public class Agent
{
      private String        nom;
      private String        prenom;
      private LocalDate     dateDeNaissance;
      private Mairie        mairie;
      
      private String        typeContrat; // TC/TP/TNC/TA
      private LocalDate     debutContrat;
      private List<String>  joursTravailles; // (lundi, mardi, mercredi, jeudi, vendredi, samedi), si TC/TP/TNC
      private List<Periode> periodesTravailles; // si TA
      
      private String        paysOrigine;
      private String        dptOrigine;
      private String        villeOrigine;
      private String        paysOrigineConjoint;
      
      private boolean       estMetropole;
      private boolean       estDomTom;
      private boolean       estCorse;      
      
      private float           joursCongesUtilises;
      private List<PeriodeConges> periodesCongesPris;
      private int           joursCongesCumules; // nombre de jours de congés cumulés l'année d'avant
      private LocalDate     demandeCongesBonifies; // dernier demande de congés bonifiés
      
      public Agent(){
          this.periodesCongesPris = new ArrayList();
          this.demandeCongesBonifies = null;
      }
      
      // public String getNom(){
          // return this.nom;
      // }
      
      // public String getPrenom(){
          // return this.prenom;
      // }
      
      // public LocalDate getDateDeNaissance(){
          // return this.dateDeNaissance;
      // }
      
      public int getAge(){ // âge au 1er janvier de l'année
          LocalDate now = LocalDate.now(ZoneId.of("Europe/Paris"));
          return Period.between(this.dateDeNaissance, LocalDate.of(now.getYear(), 1, 1)).getYears();
      }
      
      public Mairie getMairie(){
          return this.mairie;
      }
      
      public String getTypeContrat(){
          return this.typeContrat;
      }
      
      public LocalDate getDebutContrat(){
          return this.debutContrat;
      }
      
      public List<String> getJoursTravailles(){
          return this.joursTravailles;
      }
      
      public List<Periode> getPeriodesTravailles(){
          return this.periodesTravailles;
      }
      
      public String getPaysOrigine(){
          return this.paysOrigine;
      }
      
      // public String getDptOrigine(){
          // return this.dptOrigine;
      // }
      
      // public String getVilleOrigine(){
          // return this.villeOrigine;
      // }
      
      public String getPaysOrigineConjoint(){
          return this.paysOrigineConjoint;
      }
      
      public boolean getEstMetropole(){
          return this.estMetropole;
      }
      
      public boolean getEstDomTom(){
          return this.estDomTom;
      }
      
      public boolean getEstCorse(){
          return this.estCorse;
      }
      
      public float getJoursCongesUtilises(){
          return this.joursCongesUtilises;
      }
      
      public List<PeriodeConges> getPeriodesCongesPris(){
          return this.periodesCongesPris;
      }
      
      public int getJoursCongesCumules(){
          return this.joursCongesCumules;
      }
      
      public LocalDate getDemandeCongesBonifies(){
          return this.demandeCongesBonifies;
      }
      
      // Nombre de mois travaillés pour l'année en cours
      public long getMoisTravailles(){
          LocalDate now = LocalDate.now(ZoneId.of("Europe/Paris"));
          LocalDate begin = LocalDate.of(now.getYear(), 1, 1);
          long m;
          if (this.getDebutContrat().compareTo(begin) < 0){
              m = Period.between(begin, now).toTotalMonths();
          }
          else{
              m = Period.between(this.getDebutContrat(), now).toTotalMonths();
          }
          return m;
      }
      
      // Nombre de mois de service ininterrompu depuis la dernière demande de congés bonifiés ou le début du contrat si l'agent n'a pas demandé de congés bonifiés
      public long getMoisService(){
          LocalDate now = LocalDate.now(ZoneId.of("Europe/Paris"));
          if (this.demandeCongesBonifies != null){
              return Period.between(this.getDemandeCongesBonifies(), now).toTotalMonths();
          }
          else{
              return Period.between(this.getDebutContrat(), now).toTotalMonths();
          }
      }
      
      public int getJoursCongesPrisEntre(LocalDate debut, LocalDate fin){
          int n = 0;
          if (this.getPeriodesCongesPris() != null){
              for (PeriodeConges p : this.getPeriodesCongesPris()){
                  for (LocalDate d = p.getDebut(); d.isBefore(p.getFin()); d = d.plusDays(1)){
                      if (d.isAfter(debut) && d.isBefore(fin)) n += 1;
                  }
              }
          }
          return n;
      }
      
      public int getAnciennete(){
          LocalDate now = LocalDate.now(ZoneId.of("Europe/Paris"));
          return Period.between(this.getDebutContrat(), now).getYears();
      }
      
      public void setNom(String nom){
          this.nom = nom;
      }
      
      public void setPrenom(String prenom){
          this.prenom = prenom;
      }
      
      public void setDateDeNaissance(LocalDate dateDeNaissance){
          this.dateDeNaissance = dateDeNaissance;
      }

      public void setMairie(Mairie mairie){
          this.mairie = mairie;
      }
      
      public void setTypeContrat(String typeContrat){
          this.typeContrat = typeContrat;
      }
      
      public void setDebutContrat(LocalDate debutContrat){
          this.debutContrat = debutContrat;
      }
      
      public void setJoursTravailles(List<String> joursTravailles){
          this.joursTravailles = joursTravailles;
      }
      
      public void setPeriodesTravailles(List<Periode> periodesTravailles){
          this.periodesTravailles = periodesTravailles;
      }
      
      public void setPaysOrigine(String paysOrigine){
          this.paysOrigine = paysOrigine;
      }
      
      public void setDptOrigine(String dptOrigine){
          this.dptOrigine = dptOrigine;
      }
      
      public void setVilleOrigine(String villeOrigine){
          this.villeOrigine = villeOrigine;
      }
      
      public void setPaysOrigineConjoint(String paysOrigineConjoint){
          this.paysOrigineConjoint = paysOrigineConjoint;
      }
      
      public void setEstMetropole(boolean estMetropole){
          this.estMetropole = estMetropole;
      }
      
      public void setEstDomTom(boolean estDomTom){
          this.estDomTom = estDomTom;
      }
      
      public void setEstCorse(boolean estCorse){
          this.estCorse = estCorse;
      }
      
      public void setJoursCongesUtilises(float n){
          this.joursCongesUtilises = n;
      }
      
      public void setPeriodesCongesPris(List<PeriodeConges> periodesCongesPris){
          this.periodesCongesPris = periodesCongesPris;
      }
      
      public void setJoursCongesCumules(int joursCongesCumules){
          this.joursCongesCumules = joursCongesCumules;
      }
      
      public void setDemandeCongesBonifies(LocalDate demandeCongesBonifies){
          this.demandeCongesBonifies = demandeCongesBonifies;
      }
}