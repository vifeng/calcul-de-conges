package Conges_v1;

import commandes.*;

public class CongesSansSoldeTC implements CommandeI<Agent,Resultat>{
    public boolean executer(Agent a, Resultat res){
        res.setJoursDeCongesSansSolde(5*5*(12-(float)a.getMoisTravailles())/12);
        return true;
    }
}