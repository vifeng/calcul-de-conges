package Conges_v1;

import conditions.*;

public class ConditionAge implements ConditionI<Agent>
{
    private static boolean T = true;
    private String operande;
    private int age;
    
    public void setOperande(String operande){
        this.operande = operande;
    }
    
    public void setAge(int age){
        this.age = age;
    }
    
    public boolean estSatisfaite(Agent a){
        // if(T) System.out.println("Age : " + a.getAge());
        switch(this.operande) {
            case "<": return a.getAge() < this.age;
            case ">": return a.getAge() > this.age;
            case "<=": return a.getAge() <= this.age;
            case ">=": return a.getAge() >= this.age;
            case "=": return a.getAge() == this.age;
            default: return true;
        }
    }
}