package Conges_v1;

import java.time.LocalDate;

public class Periode
{
    private int semaineDebut;
    private int semaineFin;
    private int nbCinquieme;
    
    public Periode(int debut, int fin, int nb){
        this.setSemaineDebut(debut);
        this.setSemaineFin(fin);
        this.setNbCinquieme(nb);
    }
    
    public int getSemaineDebut(){
        return this.semaineDebut;
    }
    
    public int getSemaineFin(){
        return this.semaineFin;
    }
    
    public int getNbCinquieme(){
        return this.nbCinquieme;
    }
    
    public void setSemaineDebut(int debut){
        this.semaineDebut = debut;
    }
    
    public void setSemaineFin(int fin){
        this.semaineFin = fin;
    }
    
    public void setNbCinquieme(int nb){
        this.nbCinquieme = nb;
    }
}