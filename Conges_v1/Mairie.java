package Conges_v1;

public class Mairie
{
    // metropole/etranger/ville/corse
    private String nom;
    private boolean estMetropole;
    private boolean estDomTom;
    private boolean estCorse;
    
    public String getNom(){
        return this.nom;
    }
    
    public void setNom(String nom){
        this.nom = nom;
    }
    
    public boolean getEstMetropole(){
        return this.estMetropole;
    }
    
    public void setEstMetropole(boolean estMetropole){
        this.estMetropole = estMetropole;
    }
    
    public boolean getEstDomTom(){
        return this.estDomTom;
    }
    
    public void setEstDomTom(boolean estDomTom){
        this.estDomTom = estDomTom;
    }
    
    public boolean getEstCorse(){
        return this.estCorse;
    }
    
    public void setEstCorse(boolean estCorse){
        this.estCorse = estCorse;
    }
}
