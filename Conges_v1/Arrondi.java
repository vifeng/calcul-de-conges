package Conges_v1;

import commandes.*;

public class Arrondi implements CommandeI<Agent,Resultat>{
    private static boolean T = true;
    
    public boolean executer(Agent a, Resultat res){
        float n, diff;
        
        // Congés principaux
        n = res.getJoursDeConges();
        diff = n - (int)n;
        if (diff != 0){
            if (diff<=0.5) n = (int)n+0.5f;
            else n = (int)n+1;
        }
        // if(T) System.out.println("Congés principaux arrondi : "+n);
        res.setJoursDeConges(n);
        
        // Congés sans solde
        n = res.getJoursDeCongesSansSolde();
        diff = n - (int)n;
        if (diff != 0){
            if (diff<=0.5) n = (int)n+0.5f;
            else n = (int)n+1;
        }
        // if(T) System.out.println("Congés sans solde arrondi : "+n);
        res.setJoursDeCongesSansSolde(n);
        
        // Congés groupés
        n = res.getJoursDeCongesGroupes();
        diff = n - (int)n;
        if (diff != 0){
            if (diff<=0.5) n = (int)n+0.5f;
            else n = (int)n+1;
        }
        // if(T) System.out.println("Congés groupés arrondi : "+n);
        res.setJoursDeCongesGroupes(n);
        
        return true;
    }
}