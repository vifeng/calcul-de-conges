package Conges_v1;

import commandes.*;

public class CongesSansSoldeTP implements CommandeI<Agent,Resultat>{
    public boolean executer(Agent a, Resultat res){
        res.setJoursDeCongesSansSolde(5*(float)a.getJoursTravailles().size()*(12-(float)a.getMoisTravailles())/12);
        return true;
    }
}