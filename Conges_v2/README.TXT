#
verbose=true


bean.id.1=conditionTC
conditionTC.class=Conges_v2.ConditionTypeContratFr
conditionTC.property.1=contrat
conditionTC.property.1.param.1=TC

bean.id.2=operationTC
operationTC.class=Conges_v2.CongesPrincipauxTCFr

bean.id.3=commandeTC
commandeTC.class=commandes.Commande
commandeTC.property.1=condition
commandeTC.property.1.param.1=conditionTC
commandeTC.property.2=operation
commandeTC.property.2.param.1=operationTC

bean.id.4=conditionTP
conditionTP.class=Conges_v2.ConditionTypeContratFr
conditionTP.property.1=contrat
conditionTP.property.1.param.1=TP

bean.id.5=operationTP
operationTP.class=Conges_v2.CongesPrincipauxTPFr

bean.id.6=commandeTP
commandeTP.class=commandes.Commande
commandeTP.property.1=condition
commandeTP.property.1.param.1=conditionTP
commandeTP.property.2=operation
commandeTP.property.2.param.1=operationTP

bean.id.7=conditionTNC
conditionTNC.class=Conges_v2.ConditionTypeContratFr
conditionTNC.property.1=contrat
conditionTNC.property.1.param.1=TNC

bean.id.8=operationTNC
operationTNC.class=Conges_v2.CongesPrincipauxTNCFr

bean.id.9=commandeTNC
commandeTNC.class=commandes.Commande
commandeTNC.property.1=condition
commandeTNC.property.1.param.1=conditionTNC
commandeTNC.property.2=operation
commandeTNC.property.2.param.1=operationTNC

bean.id.10=conditionTA
conditionTA.class=Conges_v2.ConditionTypeContratFr
conditionTA.property.1=contrat
conditionTA.property.1.param.1=TA

bean.id.11=operationTA
operationTA.class=Conges_v2.CongesPrincipauxTAFr

bean.id.12=commandeTA
commandeTA.class=commandes.Commande
commandeTA.property.1=condition
commandeTA.property.1.param.1=conditionTA
commandeTA.property.2=operation
commandeTA.property.2.param.1=operationTA

bean.id.13=commandesCongesFranceTrue
commandesCongesFranceTrue.class=commandes.MacroCommande
commandesCongesFranceTrue.property.1=commandes
commandesCongesFranceTrue.property.1.param.1=commandeTC commandeTP commandeTNC commandeTA

bean.id.14=conditionEstFrance
conditionEstFrance.class=Conges_v2.ConditionEstFrance

bean.id.15=commandesCongesFrance
commandesCongesFrance.class=commandes.Commande
commandesCongesFrance.property.1=condition
commandesCongesFrance.property.1.param.1=conditionEstFrance
commandesCongesFrance.property.2=operation
commandesCongesFrance.property.2.param.1=commandesCongesFranceTrue


bean.id.16=operationCongesPrincipauxEspagne
operationCongesPrincipauxEspagne.class=Conges_v2.CongesPrincipauxEspagne

bean.id.17=commandeCongesPrincipauxEspagne
commandeCongesPrincipauxEspagne.class=commandes.Commande
commandeCongesPrincipauxEspagne.property.1=operation
commandeCongesPrincipauxEspagne.property.1.param.1=operationCongesPrincipauxEspagne

bean.id.18=commandesAjoutCongesPrincipaux2
commandesAjoutCongesPrincipaux2.class=Conges_v2.AjoutCongesPrincipaux
commandesAjoutCongesPrincipaux2.property.1=jours
commandesAjoutCongesPrincipaux2.property.1.param.1=2

bean.id.19=commandeEsp1
commandeEsp1.class=commandes.Commande
commandeEsp1.property.1=condition
commandeEsp1.property.1.param.1=conditionEstSeville
commandeEsp1.property.2=operation
commandeEsp1.property.2.param.1=commandesAjoutCongesPrincipaux2

bean.id.20=commandesCongesEspagneTrue
commandesCongesEspagneTrue.class=commandes.MacroCommande
commandesCongesEspagneTrue.property.1=commandes
commandesCongesEspagneTrue.property.1.param.1=commandeCongesPrincipauxEspagne commandeEsp1

bean.id.21=conditionEstEspagne
conditionEstEspagne.class=Conges_v2.ConditionEstEspagne

bean.id.22=commandesCongesEspagne
commandesCongesEspagne.class=commandes.Commande
commandesCongesEspagne.property.1=condition
commandesCongesEspagne.property.1.param.1=conditionEstEspagne
commandesCongesEspagne.property.2=operation
commandesCongesEspagne.property.2.param.1=commandesCongesEspagneTrue

bean.id.23=commandesConges
commandesConges.class=commandes.MacroCommande
commandesConges.property.1=commandes
commandesConges.property.1.param.1=commandesCongesFrance commandesCongesEspagne


bean.id.24=operationArrondi
operationArrondi.class=Conges_v2.Arrondi

bean.id.25=commandeArrondi
commandeArrondi.class=commandes.Commande
commandeArrondi.property.1=operation
commandeArrondi.property.1.param.1=operationArrondi


bean.id.26=commandes
commandes.class=commandes.MacroCommande
commandes.property.1=commandes
commandes.property.1.param.1=commandesConges commandeArrondi


# invoker
bean.id.27=invocateur
invocateur.class=commandes.Invocateur
invocateur.property.1=commande
invocateur.property.1.param.1=commandes