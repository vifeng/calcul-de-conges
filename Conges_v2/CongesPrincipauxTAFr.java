package Conges_v2;

import commandes.*;

public class CongesPrincipauxTAFr implements CommandeI<AgentFrance,Resultat>{
    public boolean executer(AgentFrance a, Resultat res){
        float n = 0;
        float d = 0;
        for (Periode p : a.getPeriodesTravailles()){
            n += (float)p.getNbCinquieme()*(float)(p.getSemaineFin() - p.getSemaineDebut());
            d += (float)(p.getSemaineFin() - p.getSemaineDebut());
        }
        if (d!=0) n = 5*n/d;
        else n = 0;
        res.addJoursDeConges(n);
        return true;
    }
}