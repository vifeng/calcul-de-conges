package Conges_v2;

import conditions.*;

public class ConditionTypeContratFr implements ConditionI<AgentFrance>
{
    private static boolean T = true;
    private String contrat;
    
    public void setContrat(String c){
        this.contrat = c;
    }
     
    public boolean estSatisfaite(AgentFrance a){
        if(T) System.out.println("TypeContrat : " + a.getTypeContrat() + ", nombre mois travaillés :  " + a.getMoisTravailles());
        return a.getTypeContrat().equals(this.contrat);
    }
}