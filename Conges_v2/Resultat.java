package Conges_v2;

public class Resultat implements java.io.Serializable
{
    private float joursDeConges;
    
    public float getJoursDeConges(){
        return this.joursDeConges;
    }
    
    public void setJoursDeConges(float n){
        this.joursDeConges = n;
    }
    
    public void addJoursDeConges(float n){
        this.joursDeConges = this.joursDeConges + n;
    }
}