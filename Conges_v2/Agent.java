package Conges_v2;

import java.time.LocalDate;

public class Agent
{     
    private String        nom;
    private String        prenom;
    private LocalDate     dateDeNaissance;
    private Mairie        mairie;
    
    public String getNom(){
        return this.nom;
      }
      
    public String getPrenom(){
        return this.prenom;
    }
      
    public LocalDate getDateDeNaissance(){
        return this.dateDeNaissance;
    }
    
    public Mairie getMairie(){
        return this.mairie;
    }
    
    public void setNom(String nom){
        this.nom = nom;
    }
      
    public void setPrenom(String prenom){
        this.prenom = prenom;
    }
      
    public void setDateDeNaissance(LocalDate dateDeNaissance){
        this.dateDeNaissance = dateDeNaissance;
    }

    public void setMairie(Mairie mairie){
        this.mairie = mairie;
    }
}
