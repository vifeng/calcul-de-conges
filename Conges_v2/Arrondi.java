package Conges_v2;

import commandes.*;

public class Arrondi implements CommandeI<Agent,Resultat>{
    private static boolean T = true;
    
    public boolean executer(Agent a, Resultat res){
        float n, diff;
        
        // Congés principaux
        n = res.getJoursDeConges();
        diff = n - (int)n;
        if (diff != 0){
            if (diff<=0.5) n = (int)n+0.5f;
            else n = (int)n+1;
        }
        if(T) System.out.println("Congés principaux arrondi : "+n);
        res.setJoursDeConges(n);
        
        return true;
    }
}