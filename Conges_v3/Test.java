package Conges_v3;

import container.Factory;
import container.ApplicationContext;
import commandes.Invocateur;
import commandes.CommandeI;
import java.util.*;
import java.time.LocalDate;

public class Test extends junit.framework.TestCase
{
    public void testCongesAgent1() throws Exception{
        ApplicationContext container = Factory.createApplicationContext("./Conges_v3/README.TXT");
        
        Mairie m = new Mairie();
        m.setNom("Lyon");
        
        Agent a = new Agent();
        a.setNom("Dupont");
        a.setPrenom("Jacques");
        a.setDateDeNaissance(LocalDate.of(1998, 1, 23));
        a.setMairie(m);
        
        Resultat resultat = new Resultat();
        
        try{
            Invocateur invocateur = container.getBean("invocateur");
            invocateur.executer(a, resultat);
        }catch(Exception e){
            e.printStackTrace();
            System.out.println("Exception ! " + e.getMessage());
        }finally{
            System.out.println("resultat: Le nombre de jours de congés de l'agent est de : "+ resultat.getJoursDeConges());
        }
    }
}