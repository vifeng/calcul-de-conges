#
verbose=true

# conges_france
bean.id.1=conges_france
conges_france.class=container.FileSystemPropsApplicationContext
conges_france.property.1=fileName
conges_france.property.1.param.1=./Conges_v1/README.TXT
conges_france.property.2=containerName
conges_france.property.2.param.1=france

# conges_espagne
bean.id.2=conges_espagne
conges_espagne.class=container.FileSystemPropsApplicationContext
conges_espagne.property.1=fileName
conges_espagne.property.1.param.1=./Conges_v3/README.TXT
conges_espagne.property.2=containerName
conges_espagne.property.2.param.1=espagne

bean.id.3=serviceLocator
serviceLocator.class=service_locator.ServiceLocator
serviceLocator.property.1=container
serviceLocator.property.1.param.1=conges_france
serviceLocator.property.2=container
serviceLocator.property.2.param.1=conges_espagne